const db = require('../helpers/conexion');
const comentarioModel = require('../sitios/sitioModel');

exports.routesConfig = function (app) {
    app.get('/sitio/:sitioId', (req, res) => {
        query = `select s.id_sitio, s.nombre, s.calificacion, s.precio, s.latitud, s.longitud, s.ciudad, s.id_pais, s.imagen, s.descripcion, c.id_categoria, c.categoria
        from sitio s
            inner join CxV CV
                on s.id_sitio = CV.id_sitio
            inner join categoria c on CV.id_categoria = c.id_categoria
            where s.id_sitio  = `+ req.params.sitioId;
        db.doQuery(query, (data) => {
            res.send(data);
        });
    });

    app.get('/sitio', (req, res) => {
        query = `select s.id_sitio, s.nombre, s.calificacion, s.precio, s.latitud, s.longitud, s.ciudad, s.id_pais, s.imagen, s.descripcion, c.id_categoria, c.categoria
        from sitio s
            inner join CxV CV
                on s.id_sitio = CV.id_sitio
            inner join categoria c on CV.id_categoria = c.id_categoria`;
        db.doQuery(query, (data) => {
            res.send({ "sitio": data });
        });
    });
	
	app.post('/sitio/nuevo', (req, res) => {
        //concatenamos los valores a insertar
        c2 = "'"+req.body['nombre']+"'";		
        c3 =c2 + ','+req.body['calificacion'];
		c3 = c3 + ','+req.body['precio'];
		c3 = c3 + ','+req.body['latitud'];
		c3 = c3 + ','+req.body['longitud'];
		c3 = c3 + ','+"'"+req.body['ciudad']+"'";
		c3 = c3 + ','+"'"+req.body['imagen']+"'";
		c3 = c3 + ','+"'"+req.body['descripcion']+"'";
		c3 = c3 + ','+req.body['id_pais'];
        //formato al inser	tar
        c4 = 'nombre, calificacion, precio, latitud, longitud, ciudad, imagen, descripcion, id_pais';
        db.insertGeneralById('sitio', c3 ,c4 , (data) => {
            res.send(data);
        });
    });
	
};